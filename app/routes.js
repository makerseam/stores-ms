const express = require('express');
const swaggerJsdoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const SwaggerConfig = require('./config/SwaggerConfig');

const swaggerSpec = swaggerJsdoc(SwaggerConfig.option);
const router = express.Router();

router.use('/api-docs', swaggerUi.serve);
router.get('/api-docs', swaggerUi.setup(swaggerSpec));

module.exports = router;
