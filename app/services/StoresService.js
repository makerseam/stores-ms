const StoresService = module.exports;
const StoresRepository = require('../repositories/StoresRepository');
const log4j = require('../utils/logger');

const defaultLogger = log4j.getLogger('MeasuresService');
console.log({ StoresService, StoresRepository, defaultLogger });
